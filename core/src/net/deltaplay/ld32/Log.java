package net.deltaplay.ld32;

import com.badlogic.gdx.Gdx;

public class Log {
    @SuppressWarnings("rawtypes")
    public static void debug(Class type, String msg) {
        Gdx.app.debug(type.getName(), msg);
    }

    @SuppressWarnings("rawtypes")
    public static void log(Class type, String msg) {
        Gdx.app.log(type.getName(), msg);
    }

    @SuppressWarnings("rawtypes")
    public static void error(Class type, String msg) {
        Gdx.app.error(type.getName(), msg);
    }
}
