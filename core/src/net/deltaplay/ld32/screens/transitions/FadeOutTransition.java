package net.deltaplay.ld32.screens.transitions;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

/**
 * Created by dpc on 23/03/15.
 */
public class FadeOutTransition extends Transition {

    @Override
    public void before() {
        prevBuffer = previousScreen.renderToBuffer();
    }

    @Override
    public void after() {
        TextureRegion region = new TextureRegion(prevBuffer.getColorBufferTexture());
        region.flip(false, true);
        Image img = new Image(region);
        nextScreen.getStage().addActor(img);
        img.toFront();
        img.addAction(Actions.sequence(Actions.fadeOut(0.5f, Interpolation.sineOut), Actions.removeActor()));
    }
}
