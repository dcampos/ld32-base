package net.deltaplay.ld32.screens.transitions;

import com.badlogic.gdx.utils.Pools;
import net.deltaplay.ld32.screens.BaseScreen;

/**
 * Created by dpc on 23/03/15.
 */
public class Transitions {
    public static final int TRANSPARENT = 0;
    public static final int FADE_OUT = 1;
    public static final int SLIDE_TO_LEFT = 2;
    public static final int SLIDE_TO_RIGHT = 3;

    public static FadeOutTransition fadeOutTransition(BaseScreen fromScreen, BaseScreen toScreen) {
        FadeOutTransition transition = Pools.get(FadeOutTransition.class, 5).obtain();
        transition.setPreviousScreen(fromScreen);
        transition.setNextScreen(toScreen);
        return transition;
    }

    public static SlideTransition slideToLeftTransition(BaseScreen fromScreen, BaseScreen toScreen) {
        return slideTransition(fromScreen, toScreen, true);
    }

    public static SlideTransition slideToRightTransition(BaseScreen fromScreen, BaseScreen toScreen) {
        return slideTransition(fromScreen, toScreen, false);
    }

    public static SlideTransition slideTransition(BaseScreen fromScreen, BaseScreen toScreen, boolean left) {
        SlideTransition transition = Pools.get(SlideTransition.class, 5).obtain();
        transition.setPreviousScreen(fromScreen);
        transition.setNextScreen(toScreen);
        transition.left = left;
        return transition;
    }

    public static TransparentTransition transparentTransition(BaseScreen fromScreen, BaseScreen toScreen) {
        TransparentTransition transition = new TransparentTransition();
        transition.setPreviousScreen(fromScreen);
        transition.setNextScreen(toScreen);
        return transition;
    }
}
