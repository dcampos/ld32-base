package net.deltaplay.ld32.screens.transitions;

import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.utils.Pool;
import net.deltaplay.ld32.screens.BaseScreen;

/**
 * Created by dpc on 23/03/15.
 */
public abstract class Transition implements Pool.Poolable {
    BaseScreen previousScreen;
    BaseScreen nextScreen;
    FrameBuffer prevBuffer;

    public Transition() {
    }

    public void doTransition() {
        before();
        nextScreen.getGame().setScreen(nextScreen);
        after();
    }

    public abstract void before();

    public abstract void after();

    @Override
    public void reset() {
        this.previousScreen = null;
        this.nextScreen = null;
        this.prevBuffer = null;
    }

    public void setNextScreen(BaseScreen nextScreen) {
        this.nextScreen = nextScreen;
    }

    public void setPreviousScreen(BaseScreen previousScreen) {
        this.previousScreen = previousScreen;
    }

}
