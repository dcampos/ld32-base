package net.deltaplay.ld32.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ProgressBar;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Timer;
import net.deltaplay.ld32.Assets;
import net.deltaplay.ld32.DisplayUtils;
import net.deltaplay.ld32.Log;
import net.deltaplay.ld32.LD32;
import net.deltaplay.ld32.screens.transitions.Transitions;

public class IntroScreen extends BaseScreen {
    private float loadingTime;
    private ProgressBar progressBar;

    public IntroScreen(LD32 game) {
        super(game, false);

        Assets.loadAll();

        Gdx.input.setInputProcessor(new InputMultiplexer(stage));
    }

    @Override
    public void render(float delta) {
        super.render(delta);
    }

    @Override
    public void init() {
        Table table = new Table();

        Texture t = new Texture(Gdx.files.internal("data/logo1.png"), true);
        t.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        Image logo = new Image(t);

        Pixmap pixmap = new Pixmap(1, (int) DisplayUtils.sizeToDpi(10), Pixmap.Format.RGB888);
        pixmap.setColor(Color.WHITE);
        pixmap.fill();

        Skin skin = new Skin();
        skin.add("white", new Texture(pixmap));

        ProgressBar.ProgressBarStyle style = new ProgressBar.ProgressBarStyle();
        style.background = skin.newDrawable("white", Color.DARK_GRAY);
        style.knobBefore = skin.newDrawable("white", Color.WHITE);

        skin.add("default-horizontal", style, ProgressBar.ProgressBarStyle.class);

        progressBar = new ProgressBar(0, 1, 0.1f, false, skin);
        progressBar.setValue(0f);

        table.setFillParent(true);
        table.add(logo).size(DisplayUtils.sizeToDpi(t.getWidth()), DisplayUtils.sizeToDpi(t.getHeight())).pad(DisplayUtils.sizeToDpi(20)).row();
        table.add(progressBar).width(DisplayUtils.sizeToDpi(logo.getWidth()));

        stage.addActor(table);

        Assets.loadAll();
        this.loadingTime = 0;

        final float interval = 0.01f;

        Timer.schedule(new Timer.Task() {
            @Override
            public void run() {
                loadingTime += interval;

                if (Assets.manager.update()) {
                    Log.log(IntroScreen.class, "Assets loaded in " + loadingTime + "s");
                    Assets.createAssets();

                    this.cancel();

                    game.setScreen(LD32.GAME_SCREEN, Transitions.FADE_OUT);

                    Gdx.input.setCatchBackKey(true);
                }

                if (progressBar != null) {
                    progressBar.setValue(Assets.manager.getProgress() / 0.5f);
                }

            }
        }, interval, interval);

        super.show();
    }
}
