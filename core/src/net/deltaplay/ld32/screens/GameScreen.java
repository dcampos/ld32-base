package net.deltaplay.ld32.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import net.deltaplay.ld32.LD32;
import net.deltaplay.melt.Assets;
import net.deltaplay.melt.Melt;
import net.deltaplay.melt.controller.WorldController;
import net.deltaplay.melt.model.World;
import net.deltaplay.melt.view.WorldView;

/**
 * Created by dpc on 15/04/15.
 */
public class GameScreen extends BaseScreen {
    private static final int UPDATES_PER_SECOND = 30;

    private World world;
    private WorldView view;
    private WorldController controller;

    private float accum;

    private OrthographicCamera camera;

    public GameScreen(LD32 melt) {
        super(melt, false);

        world = new World(Melt.SCREEN_WIDTH, Melt.SCREEN_HEIGHT);
        view = new WorldView(world, Melt.SCREEN_WIDTH, Melt.SCREEN_HEIGHT);
        stage.addActor(view);

        controller = new WorldController(world);

        createHUD();
    }

    private void createHUD() {
        Label.LabelStyle labelStyle = new Label.LabelStyle(Assets.purisa40, Color.valueOf("FFFF00"));

        Label lifeLabel = new Label("75%", labelStyle);
        float th = lifeLabel.getTextBounds().height;
        lifeLabel.setPosition(20, stage.getHeight() - th - 20);

        Label scoreLabel = new Label("00180", labelStyle);
        BitmapFont.TextBounds textBounds = scoreLabel.getTextBounds();
        scoreLabel.setPosition(stage.getWidth() / 2 - textBounds.width / 2, stage.getHeight() - textBounds.height - 20);

        Image pauseButton = new Image(Assets.pauseButton);
        pauseButton.setHeight(textBounds.height);
        pauseButton.setPosition(stage.getWidth() - pauseButton.getWidth() - 20, stage.getHeight() - pauseButton.getHeight() - 20);

        stage.addActor(lifeLabel);
        stage.addActor(scoreLabel);
        stage.addActor(pauseButton);
    }

    private void update(float delta) {
        controller.update(delta);
    }

    @Override
    public void show() {
        super.show();

        Gdx.input.setInputProcessor(new InputMultiplexer(controller, stage));
    }

    @Override
    public void render(float delta) {
        accum += delta;

        while (accum > 1.0f / UPDATES_PER_SECOND) {
            update(1.0f / UPDATES_PER_SECOND);
            accum -= 1.0f / UPDATES_PER_SECOND;
        }

        Gdx.gl.glClearColor(0f, 0f, 0f, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);


        stage.act(delta);
        stage.draw();

    }

    @Override
    protected void init() {

    }
}
