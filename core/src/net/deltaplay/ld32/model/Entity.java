package net.deltaplay.ld32.model;

import com.badlogic.gdx.math.Vector2;

public abstract class Entity<T> {
    public Vector2 position = new Vector2();
    public float width;
    public float height;
    public T bounds;
    float damage;

    public float getDamage() {
        return damage;
    }

    public T getBounds() {
        return bounds;
    }

    public Vector2 getPosition() {
        return position;
    }

    public void setPosition(Vector2 position) {
        this.position = position;
    }

    public abstract void update(float delta);

    public abstract void updateBounds();
}
