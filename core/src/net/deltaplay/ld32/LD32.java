package net.deltaplay.ld32;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.FPSLogger;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.IntMap;
import net.deltaplay.ld32.screens.BaseScreen;
import net.deltaplay.ld32.screens.GameScreen;
import net.deltaplay.ld32.screens.IntroScreen;
import net.deltaplay.ld32.screens.transitions.Transition;
import net.deltaplay.ld32.screens.transitions.Transitions;

public class LD32 extends Game {
	public static final int SCREEN_WIDTH = 480;
	public static final int SCREEN_HEIGHT = 800;
	public static final int INTRO_SCREEN = 0;
	public static final int GAME_SCREEN = 1;

    public int score;
    public Array<Integer> highscores;

	private FPSLogger fps;

	public SpriteBatch batch;

    private int currentScreen;
    private int previousScreen;
    private IntMap<Screen> screens = new IntMap<Screen>();

    public void setScreen(int type) {
        setScreen(type, Transitions.TRANSPARENT);
    }

    public void setScreen(int type, int transitionType) {
        if (type != currentScreen)
            this.previousScreen = currentScreen;

        Log.log(LD32.class, "Setting screen of type " + type);

        Transition transition;

        switch (transitionType) {
            case Transitions.FADE_OUT:
                transition = Transitions.fadeOutTransition((BaseScreen) getScreen(previousScreen), (BaseScreen) getScreen(type));
                break;

            case Transitions.SLIDE_TO_RIGHT:
                transition = Transitions.slideToRightTransition((BaseScreen) getScreen(previousScreen), (BaseScreen) getScreen(type));
                break;

            case Transitions.SLIDE_TO_LEFT:
                transition = Transitions.slideToLeftTransition((BaseScreen) getScreen(previousScreen), (BaseScreen) getScreen(type));
                break;

            default:
                transition = null;
                break;
        }

        if (transition != null) {
            transition.doTransition();
        } else {
            setScreen(getScreen(type));
        }

        this.currentScreen = type;
    }

    public Screen getScreen(int type) {

        switch (type) {


            case GAME_SCREEN:

                if (screens.get(type) == null)
                    screens.put(type, new GameScreen(this));

                return screens.get(type);

            case INTRO_SCREEN:

                if (screens.get(type) == null)
                    screens.put(type, new IntroScreen(this));

                return screens.get(type);

            default:
                break;

        }

        return null;
    }

	@Override
	public void create () {
		batch = new SpriteBatch();
		fps = new FPSLogger();

        score = 0;
        highscores = new Array<Integer>();

        setScreen(INTRO_SCREEN);
	}

	@Override
	public void render () {
		super.render();
		fps.log();
	}
}
