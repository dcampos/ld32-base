package net.deltaplay.ld32;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.ObjectMap;

public class Assets {
    public static final String ATLAS_PATH = "data/mdpi.atlas";
    public static final String PURISA40_PATH = "data/purisa40.fnt";
    public static TextureAtlas gameAtlas;
    public static TextureRegion ground;
    public static TextureRegion mountain;
    public static TextureRegion snowman;
    public static TextureRegion cloud;
    public static TextureRegion snowflake;
    public static TextureRegion circle;

    private static ObjectMap<String, TextureRegion> gameTextures;

    public static AssetManager manager = new AssetManager();
    public static BitmapFont purisa40;
    public static TextureRegion pauseButton;

    static {
        int screenDpi = DisplayUtils.getScreenDPI();
    }

    public static void loadAll() {
        manager.load(ATLAS_PATH, TextureAtlas.class);
        manager.load(PURISA40_PATH, BitmapFont.class);
   }

    public static void createAssets() {
        gameAtlas = manager.get(ATLAS_PATH, TextureAtlas.class);
        purisa40 = manager.get(PURISA40_PATH, BitmapFont.class);
        ground = gameAtlas.findRegion("ground");
        mountain = gameAtlas.findRegion("mountain");
        snowman = gameAtlas.findRegion("snowman");
        cloud = gameAtlas.findRegion("cloud");
        snowflake = gameAtlas.findRegion("snowflake");
        pauseButton = gameAtlas.findRegion("pause-button");
        circle = gameAtlas.findRegion("circle");
    }


    public static void dispose() {
        manager.clear();
        gameAtlas.dispose();
   }
}
